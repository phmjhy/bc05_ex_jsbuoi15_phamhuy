// bài 1
function tinhDiemTong() {
    var diemMonThu1 = document.getElementById('diem-mon-thu-1').value * 1;
    var diemMonThu2 = document.getElementById('diem-mon-thu-2').value * 1;
    var diemMonThu3 = document.getElementById('diem-mon-thu-3').value * 1;

    var tongDiem =0;
    if (diemMonThu1 == 0 || diemMonThu2 == 0 || diemMonThu3 == 0) {
    } else {
        tongDiem = diemMonThu1 + diemMonThu2 + diemMonThu3;
    }
    return tongDiem;
}

function tinhDiemKhuVuc() {
    const khuVuc = document.getElementById('txt-khu-vuc').value;
    var diemKhuVuc = 0;
    if (khuVuc == 0) {
        diemKhuVuc = 0;
    }
    else if (khuVuc == `A`) {
        diemKhuVuc = 2;
    } else if (khuVuc == `B`) {
        diemKhuVuc = 1;
    } else {
        diemKhuVuc = 0.5;
    }
    return diemKhuVuc;
}

function tinhDiemDoiTuong() {
    const doiTuong = document.getElementById('txt-doi-tuong').value;
    var diemDoiTuong = 0;
    if (doiTuong == 0) {
        diemDoiTuong = 0;
    }
    else if (doiTuong == 1) {
        diemDoiTuong = 2.5;
    } else if (doiTuong == 2) {
        diemDoiTuong = 1.5;
    } else {
        diemDoiTuong = 1;
    }
    return diemDoiTuong;
}

function ketQua() {
    var diemChuan = document.getElementById('txt-diem-chuan').value * 1;
    var diemTongKet = tinhDiemTong() + tinhDiemKhuVuc() + tinhDiemDoiTuong();
    if (diemTongKet >= diemChuan) {
        document.getElementById("result1").innerHTML = `Bạn đã đậu. Tổng điểm: ${diemTongKet}`;
    } else {
        document.getElementById("result1").innerHTML = `Bạn đã rớt. Tổng điểm: ${diemTongKet}`;
    }
}    
    
// bài 2

function tinhTien (){
    var hoTen = document.getElementById('txt-ho-ten').value;
    var soKw = document.getElementById('sokw').value *1;
    
    var tongTien =0;
    if (soKw<=50) {
        tongTien = soKw * 500;
    }
    else if (soKw>50 && soKw<=100) {
        tongTien = 50 * 500 + (soKw - 50) * 650;
    }
    else if (soKw>100 && soKw<=200) {
        tongTien = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
    }
    else if (soKw>200 && soKw<=350) {
        tongTien = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
    }
    else {
        tongTien = 50 * 500 + 50 * 650 + 100 * 850 + 200 * 1100 + (soKw - 350) * 1300;
    }
    document.getElementById("result2").innerHTML = `Họ tên: ${hoTen}, Tiền điện: ${tongTien}`
}